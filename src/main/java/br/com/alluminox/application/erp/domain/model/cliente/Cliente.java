package br.com.alluminox.application.erp.domain.model.cliente;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.alluminox.application.erp.domain.model.AbstractEntity;
import br.com.alluminox.application.erp.domain.model.Contato;
import br.com.alluminox.application.erp.domain.model.Localizacao;


@Entity
public class Cliente extends AbstractEntity {
	private static final long serialVersionUID = 1L;

	private String nome;	
	@Temporal(TemporalType.DATE)
	private Calendar data = Calendar.getInstance();
	
	private String cpf;
	private String rg;
	
	private String razaoSocial;
	private String cnpj;
	private String numeroInscricao;
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, orphanRemoval=true)
	private List<Contato> contatos;
	
	@OneToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	private Localizacao localizacao;
	
	public Cliente() {
		this.contatos = new ArrayList<>();
		this.localizacao = new Localizacao();
	}
	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNumeroInscricao() {
		return numeroInscricao;
	}

	public void setNumeroInscricao(String numeroInscricao) {
		this.numeroInscricao = numeroInscricao;
	}

	public List<Contato> getContatos() {
		return contatos;
	}

	public void setContatos(List<Contato> contatos) {
		this.contatos = contatos;
	}

	public Localizacao getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(Localizacao localizacao) {
		this.localizacao = localizacao;
	}

	
	private boolean containsContato(Contato contato) {
		return this.contatos.contains(contato);
	}
	
	public void addContato(Contato contato) {
		removeContato(contato);
		this.contatos.add(contato);
	}


	public void removeContato(Contato contato) {
		if(containsContato(contato))
			this.contatos.remove(contato);
	}

}

