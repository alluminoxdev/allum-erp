package br.com.alluminox.application.erp.io.dao;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.alluminox.application.erp.domain.model.Acabamento;

public class AcabamentoDAO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;
	
	public List<Acabamento> listAll() {
		return manager.createQuery("select a from Acabamento a", Acabamento.class).getResultList();
	}
}
