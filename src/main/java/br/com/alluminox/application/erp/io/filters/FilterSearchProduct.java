package br.com.alluminox.application.erp.io.filters;

import java.io.Serializable;
import java.util.List;

import br.com.alluminox.application.erp.domain.model.products.Produto;
import br.com.alluminox.application.erp.domain.model.products.TipoProduto;

public class FilterSearchProduct implements Filter<Produto>, Serializable {

	private static final long serialVersionUID = 1L;
	private TipoProduto tipoProduto;
	private String nome;
	
	private List<Produto> filterList;
	
	public TipoProduto getTipoProduto() {
		return tipoProduto;
	}
	public void setTipoProduto(TipoProduto tipoProduto) {
		this.tipoProduto = tipoProduto;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public void setFilterList(List<Produto> filterList) {
		this.filterList = filterList;
	}
	@Override
	public List<Produto> getFilterList() {
		return this.filterList;
	}
	
	
}
