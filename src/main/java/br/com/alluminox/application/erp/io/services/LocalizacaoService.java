package br.com.alluminox.application.erp.io.services;

import java.io.Serializable;

import javax.inject.Inject;

import br.com.alluminox.application.erp.custom.RestTemplate;
import br.com.alluminox.application.erp.domain.model.Localizacao;

public class LocalizacaoService implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private RestTemplate restTemplate;
	

	public Localizacao findLocalizacaoByCep(String cep) {
		return restTemplate.getForObject(String.format("https://viacep.com.br/ws/%s/json/", cep), Localizacao.class);			
	}
}
