package br.com.alluminox.application.erp.custom;

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class ObjectMapper extends com.fasterxml.jackson.databind.ObjectMapper {

	private static final long serialVersionUID = 1L;

	public ObjectMapper() {
		super.registerModules(new JavaTimeModule());
	}
}
