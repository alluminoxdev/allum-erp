package br.com.alluminox.application.erp.io.filters;

import java.util.List;

public interface Filter<T> {

	void setFilterList(List<T> list);
	List<T> getFilterList();
}
