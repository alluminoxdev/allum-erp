package br.com.alluminox.application.erp.beans;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

import br.com.alluminox.application.erp.domain.model.products.Produto;
import br.com.alluminox.application.erp.io.dao.ProdutoDAO;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

@Named
@ViewScoped
public class ReportProdutoBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private ProdutoDAO produtoDAO;
	
	@Inject
	private FacesContext context;
	
	public void report(Long id) throws SQLException {
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		Produto produto = produtoDAO.findById(id);
			
		try {

			
			Map<String, Object> parameters = new HashMap<>();
			
			// Recupero o arquivo .jasper
			InputStream resportStream = this.getClass().getResourceAsStream("/reports/orcamento.jasper");
			
			// Compilo esse Arquivo
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(resportStream);
			
			// Passo os parametros ele nos gera um arquivo compilado com os parametros setados
			// JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
			// JREmptyDataSource DATASOURCE Empty
			 JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,new JRBeanCollectionDataSource(Arrays.asList(produto)));
			// Envio pra response inputStream no OutputStream do HTTPResponse
			response.setContentType("application/pdf");
			response.setHeader("Content-disposition", "inline; filename=produtos.pdf"); // inline || attachment
			OutputStream outputStream = response.getOutputStream();
			
			// Exportar no OutputStream do HTTPResponse
			JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
			
		       outputStream.flush();
		        outputStream.close();
		        context.responseComplete();
			
		} catch (JRException | IOException e) {
			e.printStackTrace();
		}
		
	}
}

