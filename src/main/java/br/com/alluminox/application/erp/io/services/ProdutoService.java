package br.com.alluminox.application.erp.io.services;

import java.io.Serializable;

import javax.inject.Inject;

import br.com.alluminox.application.erp.domain.model.products.Produto;
import br.com.alluminox.application.erp.exceptions.ApplicationException;
import br.com.alluminox.application.erp.exceptions.DuplicatedItemException;
import br.com.alluminox.application.erp.io.dao.ProdutoDAO;
import br.com.alluminox.application.erp.io.filters.FilterSearchProduct;
import br.com.alluminox.application.erp.ui.message.Message;

public class ProdutoService implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private ProdutoDAO produtoDAO;
	
	public FilterSearchProduct findProductysBy(FilterSearchProduct filter) {
		try {
			filter.setFilterList(produtoDAO.findByNameOrTipo(filter.getNome(), filter.getTipoProduto()));
			return filter;
			
		}catch(Exception e) {
			throw new ApplicationException(e.getMessage(), e);
		}
	}
	
	public Boolean productsExists(String name) {
		try {
			return produtoDAO.existsProdutct(name);
		}catch(Exception e) {
			throw new ApplicationException(e.getMessage(), e);
		}
	}
	
	public Produto findByName(String name) {
		try {
			return produtoDAO.findByName(name);
		}catch(Exception e) {
			throw new ApplicationException(e.getMessage(), e);
		}
	}
	
	public Produto findById(Long id) {		
		try {
			return produtoDAO.findById(id);
		}catch(Exception e) {
			throw new ApplicationException(e.getMessage(), e);
		}
	}
	
	public void save(Produto produto) {
		try {
			if(produto.getId() == null && productsExists(produto.getNome())) {
				throw new DuplicatedItemException(Message.of("O produto %s já existe", produto.getNome()));
			}
			
			this.produtoDAO.save(produto);
			
		}catch(Exception e) {
			throw new ApplicationException(e.getMessage());
		}
	}
	
	
	public void deleteById(Long id) {
		try {
			
			this.produtoDAO.deleteById(id);
		}catch(Exception e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		}
	}

	public Produto findByUuid(String uuid) {
		try {
			return this.produtoDAO.findByUUID(uuid);
		}catch(Exception e) {
			e.printStackTrace();
			throw new ApplicationException("Produto não econtrado", e);
		}
	}
}
