package br.com.alluminox.application.erp.ui.converter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class LocalDateTimeConverter implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String PATTERN_UTC = "yyyy-MM-dd'T'HH:mm:ss.SSS";

	
	public static LocalDateTime getObject(String expirationTimeUTC) {
		if(expirationTimeUTC.isEmpty() || expirationTimeUTC == null) return null;
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PATTERN_UTC).withZone(ZoneId.of("UTC"));
		return LocalDateTime.parse(expirationTimeUTC, formatter);
	}

	
	
	
}
