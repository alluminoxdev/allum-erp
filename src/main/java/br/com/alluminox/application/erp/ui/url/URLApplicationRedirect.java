package br.com.alluminox.application.erp.ui.url;

import java.io.Serializable;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

public class URLApplicationRedirect implements Serializable  {
	private static final long serialVersionUID = 1L;
	private final HttpServletRequest request;
	
	private static final String BASE_APP = "/application/";
	private static final String REDIRECT = "?faces-redirect=true";
	
	@Inject
	public URLApplicationRedirect(HttpServletRequest request) {
		super();
		this.request = request;
	}
	
	public String redirectToLogin() {
		String contextPath = request.getContextPath();
		return new StringBuilder().append(contextPath).append("/index.xhtml").append(REDIRECT).toString();
	}

	public String redirectToDashboard() {
		return new StringBuilder().append(BASE_APP).append("dashboard.xhtml").append(REDIRECT).toString();
	}
	
}
