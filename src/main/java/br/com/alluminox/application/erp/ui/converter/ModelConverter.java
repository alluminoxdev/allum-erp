package br.com.alluminox.application.erp.ui.converter;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;

import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.Id;

import br.com.alluminox.application.erp.domain.model.AbstractEntity;

@FacesConverter("modelConverter")
public class ModelConverter implements Converter<Object> {
	
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if(value == null) return null;
		
		// Recupero o f:selectItems
		UISelectItems  fSelectItems = (UISelectItems ) component
					.getChildren()
					.stream()
					.filter(com -> com.getClass().isAssignableFrom(UISelectItems.class)).findFirst().get();
		
		// Recupero os valores de f:selectitem
		Collection<?> valores = (Collection<?>) fSelectItems.getValue();
		
		return valores.stream().filter(item -> {
			return getAsString(context, component, item).equals(value);
		}).findFirst().get();
				
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		// Obter o field anotado com @Id
		Field fieldAnnoted = findInField(value);
		
		// Obter o valor do field anotado com @Id
		return findValueField(value, fieldAnnoted);
	}

	private String findValueField(Object value, Field field) {
		try {
			Field fieldFiltred = getFieldOrFieldSuperrClazz(value, field);
			fieldFiltred.setAccessible(true);
			return fieldFiltred.get(value).toString();
			
		} catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	
	private Field findInField(Object value) {
		return Arrays.stream(getFieldsAndFieldsSuperClazz(value))
			.filter(field -> field.isAnnotationPresent(Id.class)).findFirst().get();
	}
	
	private Class<?> getClazzFrom(Object value) {
		 return value.getClass();
	}
	
	private boolean isSonAbstractEntity(Object value) {
		return value.getClass().getSuperclass().isAssignableFrom(AbstractEntity.class);
	}
	
	private Field getFieldOrFieldSuperrClazz(Object value, Field field) {
		Class<?> clazz = getClazzFrom(value);
		try {
			return isSonAbstractEntity(value) ? clazz.getSuperclass().getDeclaredField(field.getName())
					: clazz.getDeclaredField(field.getName());
		} catch (NoSuchFieldException | SecurityException e) {
			throw new RuntimeException(e);
		}
	}
	
	private Field[] getFieldsAndFieldsSuperClazz(Object value) {
		Class<?> clazz = getClazzFrom(value);
		return isSonAbstractEntity(value) ? clazz.getSuperclass().getDeclaredFields() : clazz.getDeclaredFields();		
	}
}
