package br.com.alluminox.application.erp.beans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import br.com.alluminox.application.erp.cdi.interceptor.transactional.Transactional;
import br.com.alluminox.application.erp.domain.model.Fornecedor;
import br.com.alluminox.application.erp.io.dao.FornecedorDAO;

@Named
@ViewScoped
public class CadastroFornecedorBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Fornecedor fornecedor;

	private FornecedorDAO fornecedorDAO;

	
	@PostConstruct
	void init() {
		this.fornecedor = new Fornecedor();
	}

	@Transactional
	public void save() {
		this.fornecedorDAO.save(fornecedor);
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
}
