package br.com.alluminox.application.erp.custom;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

public class RestTemplate extends  org.springframework.web.client.RestTemplate {

	public RestTemplate() {
		super.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
	}
	
}
