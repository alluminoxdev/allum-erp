package br.com.alluminox.application.erp.beans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.alluminox.application.erp.cdi.interceptor.transactional.Transactional;
import br.com.alluminox.application.erp.domain.model.Contato;
import br.com.alluminox.application.erp.domain.model.Localizacao;
import br.com.alluminox.application.erp.domain.model.cliente.Cliente;
import br.com.alluminox.application.erp.io.services.ClienteService;
import br.com.alluminox.application.erp.io.services.LocalizacaoService;
import br.com.alluminox.application.erp.ui.message.UIMessages;

@Named
@ViewScoped
public class CadastroCustumerBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Boolean changeCustomer;
	private Cliente cliente;
	private Contato contato;
	
	@Inject
	private UIMessages messages;
	
	@Inject
	private ClienteService clienteService;
	
	@Inject
	private LocalizacaoService localizacaoService;

	@PostConstruct
	public void init() {
		this.cliente = new Cliente();
		this.contato = new Contato();
	}
	
	
	@Transactional
	public String save() {
		try {
			this.clienteService.save(cliente);
			this.init();
			messages.addInfoFlashMessage("Cliente foi salvo com sucesso!");
		}catch(Exception e) {
			e.printStackTrace();
			messages.addErrorFlashMessage("Não foi possível salvar o Cliente");
		}
		
		return "exitToDashboard";
	}
	

	public void addContato() {
		this.cliente.addContato(this.contato);
		this.contato = new Contato();
	}


	public void removeContato(Contato contato) {
		this.cliente.removeContato(contato);
	}
	
	public void editContato(Contato contato) {
		this.contato = contato;
	}
	
	// Events
	public String backToNew() {
		init();
		return "New?faces-redirect=true";
	}
	
	public void localize() {
		try {
			String cep = this.cliente.getLocalizacao().getCep();
			cep = cep.replace("-", "");
			
			Localizacao localizacao = this.localizacaoService.findLocalizacaoByCep(cep);
			this.cliente.setLocalizacao(localizacao);
			messages.addMessage("globalMessages", "Localização Encontrada!");
		}catch(Exception e) {
			messages.addErrorMessage(e.getMessage());
		}
	}
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Contato getContato() {
		return contato;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}


	public Boolean getChangeCustomer() {
		return changeCustomer;
	}


	public void setChangeCustomer(Boolean changeCustomer) {
		this.changeCustomer = changeCustomer;
	}

}
