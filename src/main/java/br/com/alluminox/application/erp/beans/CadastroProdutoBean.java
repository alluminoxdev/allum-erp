package br.com.alluminox.application.erp.beans;


import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.alluminox.application.erp.cdi.interceptor.transactional.Transactional;
import br.com.alluminox.application.erp.domain.model.Acabamento;
import br.com.alluminox.application.erp.domain.model.Linha;
import br.com.alluminox.application.erp.domain.model.products.Produto;
import br.com.alluminox.application.erp.domain.model.products.TipoProduto;
import br.com.alluminox.application.erp.io.dao.LinhaDAO;
import br.com.alluminox.application.erp.io.services.ProdutoService;
import br.com.alluminox.application.erp.ui.RedirectView;
import br.com.alluminox.application.erp.ui.message.Message;
import br.com.alluminox.application.erp.ui.message.UIMessages;

@Named
@ViewScoped
public class CadastroProdutoBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Inject
	private LinhaDAO linhaDAO;
	
	@Inject
	private ProdutoService produtoService;
	
	@Inject
	private UIMessages messages;
	
	@Inject
	private Produto produto;	

	private String productUuid;
	
	public void init() {
		if(productUuid != null) {
			Produto find = produtoService.findByUuid(productUuid);
			if(find != null) {
				this.produto = find;
				return;
			}
		}
		
		this.produto = new Produto();
	}
	

	protected String getMessageFrom(Produto p) {
		if(produto.getId() != null) {
			return Message.of("Produto %s foi editado com sucesso", produto.getNome());
		}
		
		return Message.of("Produto %s foi salvo com sucesso", produto.getNome());
	}
	
	@Transactional
	public String save() {
		
		try {
			produtoService.save(produto);
			messages.addInfoFlashMessage(getMessageFrom(produto));
			this.produto = new Produto();
		}catch(Exception e) {
			messages.addErrorFlashMessage(e.getMessage());
		}
				
		return RedirectView.of("RegisterProduct.xhtml");
	}
	
	public List<Linha> getLinhas() {
		return this.linhaDAO.listAll();
	}
	
	public List<Acabamento> getAcabamentos() {
		return Arrays.asList(Acabamento.values());
	}

	public List<TipoProduto> getTiposProduto() {
		return Arrays.asList(TipoProduto.values());
	}

	public Produto getProduto() {
		return produto;
	}
	


	public void setProduto(Produto produto) {
		this.produto = produto;
	}


	public String getProductUuid() {
		return productUuid;
	}


	public void setProductUuid(String productUuid) {
		this.productUuid = productUuid;
	}
}
