package br.com.alluminox.application.erp.config;

import java.io.Serializable;

import javax.faces.annotation.FacesConfig;

@FacesConfig(version=FacesConfig.Version.JSF_2_3)
public class JSFConfiguration implements Serializable {
	private static final long serialVersionUID = 1L;
}
