package br.com.alluminox.application.erp.io.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.com.alluminox.application.erp.domain.model.TipoCliente;
import br.com.alluminox.application.erp.domain.model.cliente.Cliente;
import br.com.alluminox.application.erp.exceptions.ApplicationException;

public class ClienteDAO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	@Inject
	private EntityManager manager;
	
	
	public Cliente save(Cliente cliente) {
		return manager.merge(cliente);
	}
	
	public Cliente searchByCpf(String cpf) {
		return Optional.of(this.manager.createQuery("select c from cliente c where c.cpf = :pCpf", Cliente.class)
			.setParameter("pCpf", cpf)
			.getSingleResult())
		.orElseThrow(() -> new ApplicationException("Cliente não encontrado!"));
	}
	

	public Cliente searchByCnpj(String cnpj) {
		return Optional.of(this.manager.createQuery("select c from cliente c where c.cnpj like %:pCnpj%", Cliente.class)
			.setParameter("pCnpj", cnpj)
			.getSingleResult())
		.orElseThrow(() -> new ApplicationException("Cliente não encontrado!"));
	}
	

	public List<Cliente> search(String documento, TipoCliente tipoCliente) {
		List<Predicate> predicates = new ArrayList<>();
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Cliente> query = builder.createQuery(Cliente.class);
		
		Root<Cliente> root = query.from(Cliente.class);
		query = query.select(root);
		
		
		if(tipoCliente != null) {
			
			if(tipoCliente.equals(TipoCliente.PESSOA_FISICA)) {
				predicates.add(builder.equal(root.<String>get("cpf"), documento));
				
			}
			
			if(tipoCliente.equals(TipoCliente.PESSOA_JURIDICA)) {
				predicates.add(builder.equal(root.<String>get("cnpj"), documento));
			}
			
			query = query.where(predicates.toArray(new Predicate[predicates.size()]));
		}
		
		return manager.createQuery(query).getResultList();
	}
}
