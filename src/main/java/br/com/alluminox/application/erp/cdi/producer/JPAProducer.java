package br.com.alluminox.application.erp.cdi.producer;

import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@ApplicationScoped
public class JPAProducer implements Serializable {
	private static final long serialVersionUID = 1L;
	private EntityManagerFactory factoty;


	public JPAProducer() {
		this.factoty = Persistence.createEntityManagerFactory("alluminoxPU");
	}
	
	@Produces
	@RequestScoped
	public EntityManager produces() {
		return factoty.createEntityManager();
	}
	
	
	public void close(@Disposes EntityManager manager) {
		manager.close();
	}
}
