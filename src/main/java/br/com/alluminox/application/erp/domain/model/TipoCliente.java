package br.com.alluminox.application.erp.domain.model;

public enum TipoCliente {

	PESSOA_FISICA,
	PESSOA_JURIDICA
}
