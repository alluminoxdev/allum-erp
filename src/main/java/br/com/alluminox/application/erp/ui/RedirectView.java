package br.com.alluminox.application.erp.ui;

import java.io.Serializable;

public class RedirectView implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String viewName;

	
	public static String of(String viewName) {
		if(viewName != null) {
			return viewName + "?faces-redirect=true";
		}
		
		return "";
	}
	
	public RedirectView(String viewName) {
		super();
		this.viewName = viewName;
	}
	
	@Override
	public String toString() {
		if(viewName != null) {
			return viewName + "?faces-redirect=true";
		}
		
		return "";
	}
	
	
}
