package br.com.alluminox.application.erp.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.alluminox.application.erp.domain.model.Contato;
import br.com.alluminox.application.erp.domain.model.Localizacao;
import br.com.alluminox.application.erp.domain.model.TipoCliente;
import br.com.alluminox.application.erp.domain.model.cliente.Cliente;
import br.com.alluminox.application.erp.io.services.ClienteService;
import br.com.alluminox.application.erp.ui.message.UIMessages;

@Named
@ViewScoped
public class PesquisaClienteBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<Contato> contatos;
	private List<Cliente> clientes;
	
	@Inject
	private UIMessages messages;
	
	@Inject
	private ClienteService clienteService;
	
	private Localizacao localizacao;
	
	private TipoCliente tipoCliente;
	private String documento;
	
	@PostConstruct
	public void init() {
		this.localizacao = new Localizacao();
		this.contatos = new ArrayList<>();
		this.clientes = new ArrayList<>();
		
	}
	
	public void search() {
		try {
			clientes = this.clienteService.searchBy(documento, tipoCliente);
			
		}catch(Exception e) {
			messages.addErrorMessage("Não foi possivel buscar os clientes");
		}
	}
	
	public void viewLocalization(Long clienteId) {
		
	}
	
	public void viewContact(Long clienteId) {
		
	}
	
	public List<Cliente> getClientes() {
		return clientes;
	}
	
	public List<Contato> getContatos() {
		return contatos;
	}
	public Localizacao getLocalizacao() {
		return localizacao;
	}

	public TipoCliente getTipoCliente() {
		return tipoCliente;
	}
	public void setTipoCliente(TipoCliente tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	
	
	public List<TipoCliente> getTiposCliente() {
		return Arrays.asList(TipoCliente.values());
	}
}
