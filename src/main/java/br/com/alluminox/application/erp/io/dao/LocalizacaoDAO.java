package br.com.alluminox.application.erp.io.dao;

import java.io.Serializable;
import java.util.Optional;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.alluminox.application.erp.domain.model.Localizacao;

public class LocalizacaoDAO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	public Localizacao findByCep(String cep) {
		Localizacao localizacao = manager.createQuery("select l from localizacao l where l.cep = :cep", Localizacao.class)
				.setParameter("cep", cep)
				.getSingleResult();
		return Optional.ofNullable(localizacao).orElse(null);
	}
}
