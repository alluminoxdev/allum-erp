package br.com.alluminox.application.erp.io.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.com.alluminox.application.erp.domain.model.products.Produto;
import br.com.alluminox.application.erp.domain.model.products.TipoProduto;
import br.com.alluminox.application.erp.ui.message.Message;

public class ProdutoDAO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;
	
	public Produto save(Produto produto) {
		return manager.merge(produto);
	}
	

	public boolean existsProdutct(String name) {
		return findByName(name) != null ? true : false;
	}
	
	public Produto findByName(String name) {
		List<Produto> produtos = Optional.of(manager.createQuery("select p from Produto p where p.nome = :pNomeProduto", Produto.class)
				.setParameter("pNomeProduto", name)
				.getResultList()).orElse(null);
		
		if(produtos != null && !produtos.isEmpty())	
			return Optional.of(produtos.get(0)).orElse(null);
		
		return null;
	}
	
	public Produto findById(Long id) {
		return Optional.of(manager.find(Produto.class, id)).orElseThrow(() ->  new RuntimeException(Message.of("Produto %s não encontrado pelo id=", id)));
	}
	
	
	public void deleteById(Long id) {
		this.manager.remove(findById(id));
	}
	
	public List<Produto> findByNameOrTipo(String nome, TipoProduto tipoProduto) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Produto> query = builder.createQuery(Produto.class);
		
		Root<Produto> root = query.from(Produto.class);
		query = query.select(root);
		
		List<Predicate> predicateList = new ArrayList<>();
		if(nome != null && !nome.isEmpty()) {
			ParameterExpression<String> parameter = builder.parameter(String.class, "pNome");
			predicateList.add(builder.like(root.<String>get("nome"), parameter));	
		}
		
		if(tipoProduto != null) {
			ParameterExpression<TipoProduto> parameter = builder.parameter(TipoProduto.class, "pTipoProduto");
			predicateList.add(builder.equal(root.<TipoProduto>get("tipoProduto"), parameter));	
		}
		
		if(!predicateList.isEmpty()) {
			query = query.where(predicateList.toArray(new Predicate[predicateList.size()]));
		}
		
		
		TypedQuery<Produto> typedQuery = manager
			.createQuery(query);
			
		
		if(nome != null && !nome.isEmpty()) {
			typedQuery.setParameter("pNome", "%"+nome+"%");
		}
		
		if(tipoProduto != null) {
			typedQuery.setParameter("pTipoProduto", tipoProduto);
		}
		
		return typedQuery.getResultList();
	}


	public Produto findByUUID(String uuid) {
		List<Produto> list = manager
					.createQuery("select p from Produto p join fetch p.linha l  where p.uuid = :pUuid", Produto.class)
					.setParameter("pUuid", uuid).getResultList();
		if(list != null && !list.isEmpty()) {
			return list.get(0);
		}
		
		return null;  
	} 

}

