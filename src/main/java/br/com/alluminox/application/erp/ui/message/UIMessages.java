package br.com.alluminox.application.erp.ui.message;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

public class UIMessages implements Serializable {
	private static final long serialVersionUID = 1L;
	private final FacesContext facesContext;
	
	@Inject
	public UIMessages(FacesContext facesContext) {
		this.facesContext = facesContext;
	}	
	public UIMessages flash() {
		facesContext
		.getExternalContext()
		.getFlash()
		.setKeepMessages(true);
		return this;
	}
	
	public void addErrorFlashMessage(String message) {
		this.flash().addErrorMessage(message);
	}
	
	public void addInfoFlashMessage(String message) {
		this.flash().addSuccessMessage(message);
	}
	
	public void addMessage(String uuid, String message, String detail ,Severity severity) {
		facesContext.addMessage(uuid, new FacesMessage(severity, message, detail));
	}

	public void addMessage(String uuid, String message, Severity severity) {
		facesContext.addMessage(uuid, new FacesMessage(severity, message, null));
	}
	
	public void addMessage(String message, Severity severity) {
		facesContext.addMessage(null, new FacesMessage(severity, message, null));
	}
	
	public void addMessage(String uuid, String message) {
		facesContext.addMessage(uuid, new FacesMessage(FacesMessage.SEVERITY_INFO, message, null));
	}
	
	public void addMessage(String message) {
		facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, null));
	}
	
	
	
	public void addSuccessMessage(String uuid, String message) {
		facesContext.addMessage(uuid, new FacesMessage(FacesMessage.SEVERITY_INFO, message, null));
	}
	public void addSuccessMessage(String message) {
		facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, null));
	}

	public void addWarningMessage(String uuid, String message) {
		facesContext.addMessage(uuid, new FacesMessage(FacesMessage.SEVERITY_WARN, message, null));
	}
	public void addWarningMessage(String message) {
		facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, message, null));
	}
	
	public void addErrorMessage(String uuid, String message) {
		facesContext.addMessage(uuid, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
	}
	public void addErrorMessage(String message) {
		facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
	}
	
	public void addFaltalErrorMessage(String uuid, String message) {
		facesContext.addMessage(uuid, new FacesMessage(FacesMessage.SEVERITY_FATAL, message, null));
	}
	public void addFaltalErrorMessage(String message) {
		facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, message, null));
	}
		
}
