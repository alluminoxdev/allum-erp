package br.com.alluminox.application.erp.ui.message;

import java.io.Serializable;

public class Message implements Serializable {
	private static final long serialVersionUID = 1L;

	public static String of(String compose, Object ...parameters) {
		return String.format(compose, parameters);
	}

}
