package br.com.alluminox.application.erp.beans;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.alluminox.application.erp.cdi.interceptor.transactional.Transactional;
import br.com.alluminox.application.erp.domain.model.products.Produto;
import br.com.alluminox.application.erp.domain.model.products.TipoProduto;
import br.com.alluminox.application.erp.exceptions.ApplicationException;
import br.com.alluminox.application.erp.io.filters.FilterSearchProduct;
import br.com.alluminox.application.erp.io.services.ProdutoService;
import br.com.alluminox.application.erp.ui.RedirectView;
import br.com.alluminox.application.erp.ui.message.Message;
import br.com.alluminox.application.erp.ui.message.UIMessages;

@Named
@ViewScoped
public class PesquisaProdutosBean implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Inject
	private ProdutoService produtoService;
	
	@Inject
	private UIMessages messages;

	private FilterSearchProduct filter;
	
		
	@PostConstruct
	void init() {
		filter = new FilterSearchProduct();
	}
	
	public void search() {
		try {
			setFilter(produtoService.findProductysBy(getFilter()));		
		}catch(ApplicationException e) {
			messages.addErrorFlashMessage(e.getMessage());
		}
	}
	
	public String editar(Produto produto) {
		return RedirectView.of("RegisterProduct") + "&produto=" + produto.getUuid(); 
	}
	
	@Transactional
	public String delete(Produto produto) {
		try {
			produtoService.deleteById(produto.getId());
			messages.addInfoFlashMessage(Message.of("O produto %s foi deletado com sucesso!", produto.getNome()));
		}catch(Exception e) {
			e.printStackTrace();
			messages.addErrorMessage(e.getMessage());
		}
		
		return RedirectView.of("SearchProduct");
	}
	
	public List<TipoProduto> getTiposProduto() {
		return Arrays.asList(TipoProduto.values());
	}
	
	public List<Produto> getProdutos() {
		return getFilter().getFilterList();
	}

	public FilterSearchProduct getFilter() {
		return filter;
	}

	public void setFilter(FilterSearchProduct filter) {
		this.filter = filter;
	}

}
