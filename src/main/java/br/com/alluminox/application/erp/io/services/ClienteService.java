package br.com.alluminox.application.erp.io.services;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.PersistenceException;

import br.com.alluminox.application.erp.domain.model.TipoCliente;
import br.com.alluminox.application.erp.domain.model.cliente.Cliente;
import br.com.alluminox.application.erp.exceptions.ApplicationException;
import br.com.alluminox.application.erp.io.dao.ClienteDAO;

public class ClienteService implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Inject
	private ClienteDAO clienteDAO;
	
	public Cliente save(Cliente cliente) {
		try {
			if(cliente.getContatos().isEmpty()) 
				throw new ApplicationException("Insira um contato");
			return clienteDAO.save(cliente);
		}catch(PersistenceException e) {
			throw new RuntimeException(e);
		}
	}

	public List<Cliente> searchBy(String documento, TipoCliente tipoCliente) {
		try {
			return clienteDAO.search(documento, tipoCliente);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
}
