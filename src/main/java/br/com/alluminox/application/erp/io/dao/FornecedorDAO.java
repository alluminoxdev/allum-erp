package br.com.alluminox.application.erp.io.dao;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.alluminox.application.erp.domain.model.Fornecedor;

public class FornecedorDAO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	public void save(Fornecedor fornecedor) {
		manager.merge(fornecedor);
	}
	
	public List<Fornecedor> findAll() {
		return manager.createQuery("select f from Fornecedor f",Fornecedor.class).getResultList();
	}
	
	public Fornecedor findById(Long id) {
		return manager.find(Fornecedor.class, id);
	}
	
	public void remove(Long id) {
		manager.remove(findById(id));
	}
}
