package br.com.alluminox.application.erp.domain.model.products;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;

import br.com.alluminox.application.erp.domain.model.AbstractEntity;
import br.com.alluminox.application.erp.domain.model.Acabamento;
import br.com.alluminox.application.erp.domain.model.Linha;

@Entity
public class Produto extends AbstractEntity {
	private static final long serialVersionUID = 1L;

	private String uuid;
	private String nome;
	private String descricao;
	private String codigoMercado;
	
	@Enumerated(EnumType.STRING)
	private TipoProduto tipoProduto;
	
	private String codigoBarras;
	
	private BigDecimal desconto;
	
	private BigDecimal precoCompra;
	
	private BigDecimal precoVenda;

	@Enumerated(EnumType.STRING)
	private Acabamento acabamento;

	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private Linha linha;

	@PrePersist
	public void prePersist() {
		this.uuid = UUID.randomUUID().toString();
	}


	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCodigoMercado() {
		return codigoMercado;
	}
	public void setCodigoMercado(String codigoMercado) {
		this.codigoMercado = codigoMercado;
	}

	public TipoProduto getTipoProduto() {
		return tipoProduto;
	}
	public void setTipoProduto(TipoProduto tipoProduto) {
		this.tipoProduto = tipoProduto;
	}

	public String getCodigoBarras() {
		return codigoBarras;
	}
	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}

	public BigDecimal getDesconto() {
		return desconto;
	}
	public void setDesconto(BigDecimal desconto) {
		this.desconto = desconto;
	}

	public BigDecimal getPrecoCompra() {
		return precoCompra;
	}
	public void setPrecoCompra(BigDecimal precoCompra) {
		this.precoCompra = precoCompra;
	}

	public BigDecimal getPrecoVenda() {
		return precoVenda;
	}
	public void setPrecoVenda(BigDecimal precoVenda) {
		this.precoVenda = precoVenda;
	}

	public Acabamento getAcabamento() {
		return acabamento;
	}
	public void setAcabamento(Acabamento acabamento) {
		this.acabamento = acabamento;
	}

	public Linha getLinha() {
		return linha;
	}
	public void setLinha(Linha linha) {
		this.linha = linha;
	}


	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}

	
	
	
}
