package br.com.alluminox.application.erp.exceptions;

public class DuplicatedItemException extends RuntimeException {

	public DuplicatedItemException() {
		super();
	}
	
	public DuplicatedItemException(String message) {
		super(message);
	}
	
	public DuplicatedItemException(String message, Throwable ex) {
		super(message, ex);
	}
	
	private static final long serialVersionUID = 1L;

}
