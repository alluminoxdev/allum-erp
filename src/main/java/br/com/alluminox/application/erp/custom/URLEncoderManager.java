package br.com.alluminox.application.erp.custom;


import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class URLEncoderManager implements Serializable  {
	private static final long serialVersionUID = 1L;
	
	public static String encodeUTF8(String value) {
		try {
			return URLEncoder.encode(value, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new AssertionError("Encoding UTF-8 not support by JVM");
		}
	}
	
	public static String decodeUTF8(String value) {
		try {
		
			return 	URLDecoder.decode(value, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new AssertionError("Encoding UTF-8 not support by JVM");
		}
	}

	
}
