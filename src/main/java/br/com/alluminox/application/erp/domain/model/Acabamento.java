package br.com.alluminox.application.erp.domain.model;

import java.math.BigDecimal;

public enum Acabamento  {
	
	ANODIZACAO("4.50"),
	BRANCO("3.50"),
	TEXTURIZADO("18.00");
	
	private String value;
	
	private Acabamento(String value) {
		this.value = value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	public BigDecimal getBigDecimalValue() {
		return new BigDecimal(getValue());
	}
	
}
